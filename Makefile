DOCKER_VERSIONS=stable 27 27.4 27.4.0
BUILDX_VERSIONS=0.19.2
EXTRA_ARGS?=

build:
	@for DOCKER_VERSION in $(DOCKER_VERSIONS); do \
		for BUILDX_VERSION in $(BUILDX_VERSIONS); do \
			docker buildx build ${EXTRA_ARGS} --build-arg DOCKER_VERSION=$$DOCKER_VERSION --build-arg BUILDX_VERSION=$$BUILDX_VERSION --load -t aazario/docker-with-buildx:$$DOCKER_VERSION-$$BUILDX_VERSION . ; \
		done ; \
	done
