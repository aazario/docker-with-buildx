# docker-with-buildx

Docker image with buildx installed, this image is used in Gitlab CI/CD to build and push the images on DockerHub. 

Configuration example for .gitlab-ci.yml:

```
image: aazario/docker-with-buildx:latest

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2

services:
  - name: docker:dind

build:
  stage: build
  script:
    - docker login -u "$DOCKER_USERNAME" -p "$DOCKER_PASSWORD"
    - docker buildx create --use --driver-opt network=host
    - docker buildx build --push --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --tag $DOCKER_USERNAME/xxxxxx:latest .
```