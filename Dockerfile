ARG DOCKER_VERSION=stable
ARG BUILDX_VERSION=0.19.2

FROM docker/buildx-bin:${BUILDX_VERSION} AS fetcher

FROM docker:${DOCKER_VERSION}

LABEL org.label-schema.docker.dockerfile=/Dockerfile org.label-schema.vcs-type=Git org.label-schema.vcs-url=https://gitlab.com/aazario/docker-with-buildx.git

COPY --from=fetcher /buildx /usr/libexec/docker/cli-plugins/docker-buildx
